let
  nixpkgs = import <nixpkgs> {};
in
import ./release.nix { nixpkgs = nixpkgs; }
