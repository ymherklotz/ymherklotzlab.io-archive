{-# LANGUAGE OverloadedStrings #-}

module Site.Pandoc
  ( pandocTOCCompiler,
    pandocFeedCompiler,
    pandocCompiler,
  )
where

import Hakyll hiding (pandocCompiler)
import Site.Pygments
import Site.Types
import Text.Pandoc

pandocTOCCompiler :: Streams -> Compiler (Item String)
pandocTOCCompiler streams =
  pandocCompilerWithTransformM
    defaultHakyllReaderOptions
    (defaultHakyllWriterOptions {writerTableOfContents = True})
    (pygments streams)

pandocFeedCompiler :: Compiler (Item String)
pandocFeedCompiler =
  pandocCompilerWith
    defaultHakyllReaderOptions
    $ defaultHakyllWriterOptions {writerHTMLMathMethod = PlainMath}

pandocCompiler :: Streams -> Compiler (Item String)
pandocCompiler streams =
  pandocCompilerWithTransformM
    defaultHakyllReaderOptions
    defaultHakyllWriterOptions
    (pygments streams)
