module Site.Types
  ( Streams,
    Content (..),
  )
where

import Data.ByteString.Char8 (ByteString)
import Hakyll (Context, Pattern)
import System.IO.Streams (InputStream, OutputStream)

-- the pigments server streams
type Streams = (OutputStream ByteString, InputStream ByteString)

-- the information for a kind of content, e.g. notes
data Content
  = Content
      { contentPattern :: Pattern,
        contentRoute :: String,
        contentTemplate :: String,
        contentContext :: Context String,
        contentLayoutContext :: Context String
      }
