{-# LANGUAGE OverloadedStrings #-}

module Site.News (parseNews) where

import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Lazy as T (toStrict)
import Data.Time.Calendar (showGregorian)
import qualified TOML
import Text.Blaze.Html ((!), Html)
import Text.Blaze.Html.Renderer.Text (renderHtml)
import qualified Text.Blaze.Html5 as Html
import qualified Text.Blaze.Html5.Attributes as Attr

formatString :: TOML.Value -> Text
formatString (TOML.String t) = t
formatString (TOML.DayV d) = T.pack $ showGregorian d
formatString _ = error "formatString: Not implemented"

getElement :: Text -> [(Text, TOML.Value)] -> TOML.Value
getElement t ((k, v) : b)
  | k == t = v
  | otherwise = getElement t b
getElement _ _ = error "getElement: Not implemented"

formatTable :: TOML.Value -> Html
formatTable (TOML.Table t) =
  Html.tr $ do
    (Html.td ! Attr.class_ "news-date") . Html.text . formatString $
      (getElement "date" t)
    Html.td . Html.preEscapedText . formatString $
      (getElement "event" t)
formatTable _ = error "formatTable: Not implemented"

formatTOML :: TOML.Value -> [Text]
formatTOML (TOML.List l) =
  T.toStrict . renderHtml . formatTable <$> l
formatTOML _ = error "formatTOML: Not implemented"

parseNews :: String -> IO [Text]
parseNews t = do
  newsf <- T.readFile t
  case TOML.parseTOML newsf of
    Left e -> error ("Could not parse news.toml : " <> show e)
    Right t' -> return . formatTOML $ getElement "news" t'
