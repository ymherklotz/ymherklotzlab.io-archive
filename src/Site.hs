{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Char as Char
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Hakyll hiding (pandocCompiler)
import Site.News
import Site.Pandoc
import Site.Pygments
import Site.Types

config :: Configuration
config =
  defaultConfiguration
    { destinationDirectory = "public"
    }

capitalised :: String -> String
capitalised (head' : tail') = Char.toUpper head' : map Char.toLower tail'
capitalised [] = []

postCtx :: Context String
postCtx = dateField "date" "%B %e, %Y" <> defaultContext

postsPattern :: Pattern
postsPattern = "blog/*.md" .||. "blog/*.org"

feedConfig :: FeedConfiguration
feedConfig =
  FeedConfiguration
    { feedTitle = "Yann's Blog",
      feedDescription = "Personal blogs about my research and tech in general.",
      feedAuthorName = "Yann Herklotz",
      feedAuthorEmail = "yann@yannherklotz.com",
      feedRoot = "https://yannherklotz.com"
    }

matchCopyAll :: Pattern -> Rules ()
matchCopyAll str = match str $ do
  route idRoute
  compile copyFileCompiler

processPost :: Streams -> Rules ()
processPost streams = do
  route $ setExtension "html"
  compile $
    pandocTOCCompiler streams
      >>= loadAndApplyTemplate "templates/post.html" postCtx
      >>= saveSnapshot "content"
      >>= loadAndApplyTemplate "templates/default.html" postCtx
      >>= relativizeUrls

replaceChar :: (Eq a) => a -> [a] -> [a] -> [a]
replaceChar _ _ [] = []
replaceChar char other (a : x)
  | char == a = other <> replaceChar char other x
  | otherwise = a : replaceChar char other x

compileTags :: Tags -> [Char] -> Pattern -> Rules ()
compileTags tags tagStr tagsPattern = do
  route idRoute
  compile $ do
    posts <- recentFirst =<< loadAll tagsPattern
    postItemTpl <- loadBody "templates/post-list-item.html"
    postList <- applyTemplateList postItemTpl postCtx posts
    tagCloud <- renderTagCloud 70.0 200.0 tags
    let tagCtx =
          constField "title" (capitalised tagStr)
            <> constField "postlist" postList
            <> constField "tag" (capitalised tagStr)
            <> constField "rightcolumn" tagCloud
            <> defaultContext
    makeItem ""
      >>= loadAndApplyTemplate "templates/tags.html" tagCtx
      >>= loadAndApplyTemplate "templates/default.html" tagCtx
      >>= relativizeUrls

main :: IO ()
main = do
  streams <- pygmentsServer
  news <- parseNews "news.toml"
  hakyllWith config $ do
    tags <- buildTags postsPattern (fromCapture "tags/*.html")
    mapM_
      matchCopyAll
      [ "images/**",
        "docs/**",
        "papers/**",
        "fonts/**",
        "favicon*",
        "js/**"
      ]
    match "css/*" $ route idRoute >> compile compressCssCompiler
    match postsPattern $ processPost streams
    match "blog.md" $ do
      route $ constRoute "blog/index.html"
      compile $ do
        blog <- recentFirst =<< loadAll "blog/*.md"
        tagCloud <- renderTagCloud 70.0 200.0 tags
        let blogCtx =
              listField "blog" postCtx (return blog)
                <> constField "rightcolumn" tagCloud
                <> defaultContext
        pandocCompiler streams
          >>= applyAsTemplate blogCtx
          >>= loadAndApplyTemplate "templates/default.html" blogCtx
          >>= relativizeUrls
    match "news.md" $ do
      route $ constRoute "news/index.html"
      compile $ do
        let newsCtx =
              constField "news" (concat $ T.unpack <$> news)
                <> defaultContext
        pandocCompiler streams
          >>= applyAsTemplate newsCtx
          >>= loadAndApplyTemplate "templates/default.html" newsCtx
          >>= relativizeUrls
    match "photos.md" $ do
      route $ constRoute "photos/index.html"
      compile $ do
        let photosCtx =
              constField "photos" "true"
                <> defaultContext
        pandocCompiler streams
          >>= applyAsTemplate photosCtx
          >>= loadAndApplyTemplate "templates/default.html" photosCtx
          >>= relativizeUrls
    create ["tags/index.html"] $ do
      route idRoute
      compile $ do
        tagCloud <- renderTagList tags
        let tagCtx =
              constField "body" (replaceChar ',' "<br>" tagCloud)
                <> constField "title" "Tags"
                <> defaultContext
        makeItem ""
          >>= loadAndApplyTemplate "templates/default.html" tagCtx
          >>= relativizeUrls
    match ("*.md" .||. "*.org") $ do
      route $ setExtension "html"
      compile $ do
        blog <- recentFirst =<< loadAll postsPattern
        let indexCtx =
              listField "blog" postCtx (return $ take 5 blog)
                <> constField "news" (concat $ T.unpack <$> take 5 news)
                <> constField "title" "Home"
                <> defaultContext
        pandocCompiler streams
          >>= applyAsTemplate indexCtx
          >>= loadAndApplyTemplate "templates/default.html" indexCtx
          >>= relativizeUrls
    match "templates/*" $ compile templateBodyCompiler
    create ["atom.xml"] $ do
      route idRoute
      compile $ do
        let feedCtx = postCtx <> bodyField "description"
        blog <- fmap (take 10) . recentFirst =<< loadAllSnapshots postsPattern "content"
        renderAtom feedConfig feedCtx blog
    tagsRules tags $ compileTags tags
