PAPER_BIB := blog/templates/papers.html
PAPER_BIB_OTHER := blog/templates/papers_other.html
PREFIX := $(PWD)/bin
SRC := $(shell find src -type f -name "*.hs")

all: public

$(PREFIX)/ymherklotz: $(SRC)
	cabal install --overwrite-policy=always --installdir=$(PREFIX)

public: blog/public
	rm -rf public
	cp -r blog/public public

blog/public: $(PREFIX)/ymherklotz $(PAPER_BIB)
	(cd blog; $(PREFIX)/ymherklotz build)

$(PAPER_BIB): blog/yann.bib
	bibtex2html -o blog/templates/papers -use-keys -dl -linebreak \
        -noabstract -nokeywords -nobibsource -nofooter -nodoc \
        --named-field url_video video --named-field url_tex tex \
        --named-field url_slides slides --named-field url_press press \
        --named-field url_poster poster --named-field url_manuscript \
        pdf --named-field url_blog_post blog --named-field \
        url_artifact artifact blog/yann.bib
	sed -i -r -e 's:\[(.*)\]:<b>\1</b>:g' $(PAPER_BIB)
	sed -i -r -e 's:Yann Herklotz:<b>Yann Herklotz</b>:g' $(PAPER_BIB)

$(PAPER_BIB_OTHER): blog/yann_other.bib
	bibtex2html -o blog/templates/papers_other -use-keys -dl -linebreak \
        -noabstract -nokeywords -nobibsource -nofooter -nodoc \
        --named-field url_video video --named-field url_tex tex \
        --named-field url_slides slides --named-field url_press press \
        --named-field url_poster poster --named-field url_manuscript \
        pdf --named-field url_blog_post blog --named-field \
        url_artifact artifact blog/yann_other.bib
	sed -i -r -e 's:\[(.*)\]:<b>\1</b>:g' $(PAPER_BIB_OTHER)
	sed -i -r -e 's:Yann Herklotz:<b>Yann Herklotz</b>:g' $(PAPER_BIB_OTHER)

clean:
	rm -rf public
	rm -rf blog/public
	rm -rf blog/_cache
	rm -rf dist-newstyle
	rm -f $(PAPER_BIB)
	rm -rf $(PREFIX)

.PHONY: clean
