<img src="/images/portrait.jpg" alt="Profile picture" class="profile-picture" />

<span class="first-letter">H</span>i! I'm currently a first year PhD student in the Circuits and Systems group at Imperial College London, supervised by [John Wickerson](https://johnwickerson.github.io).

My research focuses on formalising the process of converting high-level programming language descriptions to correct hardware that is functionally equivalent to the input. This process is called high-level synthesis (HLS), and allows software to be turned into custom accelerators automatically, which can then be placed on field-programmable gate arrays (FPGAs).  An implementation in the [Coq](https://coq.inria.fr/) theorem prover called Vericert can be found on [Github](https://github.com/ymherklotz/vericert).

I have also worked on random testing for FPGA synthesis tools. [Verismith](https://github.com/ymherklotz/verismith) is a fuzzer that will randomly generate a Verilog design, pass it to the synthesis tool, and use an equivalence check to compare the output to the input. If these differ, the design is automatically reduced until the bug is located.

## Publications

$partial("templates/papers.html")$

## Other Papers and Posters

$partial("templates/papers_other.html")$

## Blog

$partial("templates/post-list.html")$

[more...](/blog/)

## News

$partial("templates/news.html")$

[more...](/news/)
