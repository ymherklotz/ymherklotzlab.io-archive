---
title: Noise Silencer
abstract: A real-time noise cancelling processor that reduces noise from audio.
code: https://github.com/ymherklotz/NoiseSilencer
tags: dsp
---

<span class="first-letter">T</span>he [paper](/docs/RTDSP_paper.pdf) and the code are also available.

We wrote a Real Time DSP system on a TI chip that would reduce any noise from
any signal in real time, using approximations of what the noise spectrum looks
like, and subtracting that from the original spectrum of the signal.
