---
title: Emotion Classification using Images
abstract: A convolutional neural network was trained to classify images based on 6 emotions, achieving an accuracy of about 60%.
code: https://github.com/ymherklotz/EmotionClassifier
tags: 
---

<span class="first-letter">W</span>e wrote a Convolutional Neural Network that would classify images into the 6
base emotions. We used tensorflow and python to design the network and train it.
