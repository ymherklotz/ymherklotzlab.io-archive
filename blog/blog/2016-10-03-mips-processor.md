---
title: "Mips Processor"
abstract: Implemented a MIPS I CPU simulator in C++ which can execute assembly code.
code: https://github.com/ymherklotz/MipsCPU
tags: hardware
---

<span class="first-letter">T</span>he MIPS I architecture is quite simple when comparing it to different RISC
CPU's such as ARM. The five pipeline stages have distinct purposes and each
instruction has a clear meaning. It also does not contain that many
optimisations, which leads to loads and branches introducing delay
slots. However, this makes it perfect to implement for a simulation of a CPU.

I implemented a clock cycle accurate simulation of a MIPS I CPU, which supports
most MIPS I instructions.
