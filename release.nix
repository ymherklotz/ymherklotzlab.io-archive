{ nixpkgs ? import <nixpkgs> {} } :
let
  inherit (nixpkgs.pkgs.lib.trivial) flip pipe;
  inherit (nixpkgs.pkgs.haskell.lib) appendPatch appendConfigureFlags dontCheck;

  hakyllFlags = [ "-f" "watchServer" "-f" "previewServer" ];

  #haskellPackages = nixpkgs.pkgs.haskell.packages.${compiler}.override {
  #  overrides = hpNew: hpOld: {
  #    hakyll =
  #      pipe
  #        hpOld.hakyll
  #        [ (flip appendConfigureFlags hakyllFlags)
  #          dontCheck
  #        ];
  #
  #  };
  #};
  ymherklotz = nixpkgs.pkgs.haskellPackages.callCabal2nix "ymherklotz" (./.) {};
in
ymherklotz
