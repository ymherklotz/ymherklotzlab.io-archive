with import <nixpkgs> {};
let site = ( import ./. {} ); in
mkShell {
  buildInputs = site.env.nativeBuildInputs ++
                [ wget bibtex2html cabal-install
                  python3 python3Packages.pygments
                  coreutils glibcLocales
                ];
  LANG = "en_US.UTF-8";
  LC_ALL = "en_US.UTF-8";
}
