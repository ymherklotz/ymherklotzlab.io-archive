#!/usr/bin/env python3

import dateutil.parser as dateparser
import flickr_api
import sys
import tqdm

flickr_api.set_keys(api_key="a6279cd425b3c0ac8945a6cbba338931", api_secret="e18def6e7c4311aa04")


def encodePhoto(photo):
    return '<a target="_blank" href="{source}"><img src="{thumbnail}" alt="{title}"></a>'.format(**photo)


def encodeColumn(photos):
    return """<div class="gallery-column">
{}
</div>""".format("\n".join(map(encodePhoto, photos)))


def encodeRow(size, photos):
    split_photos = [[] for _ in range(size)]
    for s, p in enumerate(photos):
        if p is not None:
            if p["orientation"] == "portrait":
                photos.insert(s + size, None)
            split_photos[s % size].append(p)
    return """<div class="gallery-{}">
<div class="gallery-row">
{}
</div>
</div>""".format(size, "\n".join(map(encodeColumn, split_photos)))


def main(fn=None):
    user = flickr_api.Person.findByUserName("ymherklotz")
    photos = user.getPhotos()
    data = []
    print("Updating photos...", file=sys.stderr)
    for photo in tqdm.tqdm(photos, ascii=True):
        photoinfo = photo.getInfo()
        photosizes = photo.getSizes()
        data.append({
            "title": photo.title,
            "description": photoinfo["description"],
            "thumbnail": photosizes["Medium 800"]["source"],
            "source": photosizes["Large 2048"]["source"],
            "date": dateparser.parse(photoinfo["taken"]),
            "orientation": "landscape" if photosizes["Small 400"]["width"] > photosizes["Small 400"]["height"] else "portrait"
        })
    sorted_data = sorted(data, key=lambda s: s["date"], reverse=True)
    encoded = map(lambda s: encodeRow(s, sorted_data), [4, 2, 1])
    if fn is None:
        print("\n".join(encoded))
    else:
        with open(fn, "w") as f:
            f.writelines(encoded)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        main()
